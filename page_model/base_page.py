from connection_setup import Connection
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.common.by import By
from selenium.common.exceptions import TimeoutException
import selenium.webdriver.support.ui as ui
import pytest
import time

class BasePage():

    driver = Connection.driver

    def __init__(self):
        pass

    def find_mode(self, element, mode=None):
        self.wait_for_overlay_dissapear()
        if mode == "visibility":
            condition = ec.visibility_of_element_located(element)
        elif mode == "invisibility":
            condition = ec.invisibility_of_element_located(element)
            print("wait this element to be dissapear {}".format(element))
        else:
            condition = ec.presence_of_element_located(element)
        return condition

    def find_element(self, element, time_out=10, mode=None):
        condition = self.find_mode(element, mode)
        try:
            result = WebDriverWait(self.driver, time_out).until(condition)
            return result
        except TimeoutException:
            pytest.fail(msg="This element couldn't be found : {} ".format(element))

    def find_elements(self, element, time_out=10):
        '''this method can be used for get set of data like list tasks, goals and connections'''
        try:
            result = WebDriverWait(self.driver, time_out).until(ec.presence_of_all_elements_located(element))
            return result
        except TimeoutException:
            pytest.fail(msg="This element couldn't be found : {} ".format(element))

    def verify_elements(self, elements, mode=None):
        '''param mode: "invisibility", "visibility", default : presence_of_element_located '''
        for element in elements:
            result = self.find_element(element, time_out=15, mode=mode)
            if result != False:
                print("element found : {} ".format(element))

    def wait_for_overlay_dissapear(self):
        overlay = (By.XPATH, "//div[@class='overlay']")
        WebDriverWait(self.driver, 5).until(ec.invisibility_of_element_located(overlay))

    def wait_clickable(self,target):
        wait = ui.WebDriverWait(self.driver, 30)
        wait.until(ec.element_to_be_clickable(target))

    def wait_selected(self,target):
        wait = ui.WebDriverWait(self.driver, 30)
        wait.until(ec.element_located_to_be_selected(target))

    def verify_visibility(self, element):
        return WebDriverWait(self.driver, 5).until(ec.visibility_of_element_located(element))

    def get_data(self, element, time_out=5):
        '''this method can be used for get set of data like list tasks, goals and connections'''
        try:
            result = WebDriverWait(self.driver, time_out).until(ec.presence_of_all_elements_located(element))
            return result
        except TimeoutException:
            print("Data not found or empty")
            return None


    def swipe_to_bottom(self, target_element=None):
        # asus y2 = -100

        if target_element == None:
            try:
                self.driver.swipe(350, 700, 350, 50, 1000)
                print("swipe success")
            except:
                print("swipe failed")
        else:
            print("Swipe to element : {}".format(target_element))

            n = 4
            while n > 0:
                print("searching...")
                is_element_visible = self.find_element(target_element, time_out=1)
                if is_element_visible == None:
                    self.driver.swipe(350, 700, 350, 50, 1000)
                    n = n - 1
                    # print("swipe count : {} left".format(n))
                else:
                    print("Element found")
                    return
            else:
                print("swipe 4 times but element not found")