from page_model import BasePage
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.action_chains import ActionChains
from selenium.common.exceptions import TimeoutException, NoSuchElementException, ElementNotVisibleException
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec
import time

class Dashboard(BasePage):

    loc_home_label = (By.CSS_SELECTOR, "div.page-title")
    loc_company_name = (By.XPATH, "//span[@class='company-name']")

    '''warning popup'''
    loc_user_name = (By.CSS_SELECTOR, "div.name")
    loc_warning_pop_up = (By.XPATH, "//div[@class='sweet-alert showSweetAlert visible']")
    loc_update_later_button = (By.XPATH, "//button[@class='cancel' and contains(text(),'Update later')]")

    '''release notes'''
    loc_release_notes = (By.XPATH, "//div[@class='article-release-note']")
    loc_close_relase_note = (By.XPATH, "//span[@class='k-icon k-i-close']")

    '''mentee menu'''
    loc_goals_title = (By.XPATH, "//h2[contains(text(), 'Goals')]")
    loc_WH_title = (By.XPATH, "//h2[contains(text(), 'Happening')]")
    loc_tasks_title = (By.XPATH, "//h2[contains(text(), 'Tasks')]")
    loc_projects_title = (By.XPATH, "//h2[contains(text(), 'Projects')]")
    loc_goal_expander = (By.XPATH, "//h2[@class='widget-header' and contains(text(),'Goals')]/a")
    loc_tasks_expander = (By.XPATH, "//h2[@class='widget-header' and contains(text(),'Tasks')]/a")
    loc_search_mentor = (By.XPATH, "//span[contains(text(),'Search Mentor')]")

    '''mentor menu'''
    loc_my_mentee_label = (By.XPATH, "//h2[contains(text(), 'My Mentee')]")
    loc_my_mentee_expander = (By.XPATH, "//h2[@class='widget-header' and contains(text(),'My Mentee')]/a")
    loc_goals_and_tasks_title = (By.XPATH, "//h2[contains(text(), 'Goals & Tasks')]")
    loc_goals_and_tasks_expander = (By.XPATH, "//h2[contains(text(), 'Goals & Tasks')]/a")

    '''admin menu'''
    loc_my_network_label = (By.XPATH, "//h2[contains(text(), 'My Network')]")
    loc_user_growth_label = (By.XPATH, "//h2[contains(text(), 'User Growth')]")

    '''menu'''
    loc_projects_menu = (By.ID, "2")
    loc_messages_menu = (By.ID, "21")
    loc_bulletin_menu = (By.ID, "12")
    loc_dynamicform_menu = (By.ID, "28")
    loc_my_connection_menu = (By.XPATH, "//span[contains (text(),'My Connections')]")
    loc_log_out_menu = (By.XPATH, "//span[contains(text(),'Logout')]")

    alias_mentifi = "AUMEN"
    alias_hub = "Hub3c"

    def __init__(self):
        super().__init__()

    def verify_information_pop_up(self):
        try:
            warn_popup = self.verify_visibility(self.loc_warning_pop_up)
            print("[DASHBOARD PAGE] there is warning pop up containing messages :\n{}".format(warn_popup.text))
            self.wait_for_overlay_dissapear()
            self.find_element(self.loc_update_later_button).click()
        except TimeoutException:
            print("[DASHBOARD PAGE] No pop up warning messages")

    def verify_release_notes_pop_up(self):
        try:
            release_note = self.verify_visibility(self.loc_release_notes)
            print("[DASHBOARD PAGE] Release note content : \n{}".format(release_note.text))
            self.wait_for_overlay_dissapear()
            release_note.click()
            actions = ActionChains(self.driver)
            actions.send_keys(Keys.ESCAPE).perform()
        except TimeoutException:
            print("[DASHBOARD PAGE] No release note pop up")

    def get_logged_user_name(self):
        user_name = self.find_element(self.loc_user_name).text
        print("[DASHBOARD PAGE] user name displayed: {}".format(user_name))

    def verify_dashboard_elements(self, alias):
        home = self.find_element(self.loc_home_label)
        assert home.text == "Home"

        company_name = self.find_element(self.loc_company_name)
        print("[DASHBOARD PAGE] Company Name label, actual value = {}".format(company_name.text))
        assert company_name.text == alias

    def verify_mentor_dashboard(self):
        self.verify_dashboard_elements(self.alias_mentifi)
        mentor_menu = [self.loc_my_mentee_label, self.loc_WH_title, self.loc_goals_and_tasks_title]
        self.verify_elements(mentor_menu)

    def verify_mentee_dashboard(self):
        self.verify_dashboard_elements(self.alias_mentifi)
        mentee_menu = [self.loc_goals_title, self.loc_tasks_title, self.loc_WH_title, self.loc_projects_title]
        self.verify_elements(mentee_menu)

    def verify_admin_dashboard(self):
        self.verify_dashboard_elements(self.alias_mentifi)
        admin_menu = [self.loc_my_network_label, self.loc_user_growth_label]
        self.verify_elements(admin_menu)

    def verify_nonmentifi_dashboard(self):
        self.verify_dashboard_elements(self.alias_hub)
        nonmentifi_menu = [self.loc_projects_menu, self.loc_messages_menu, self.loc_bulletin_menu]
        self.verify_elements(nonmentifi_menu)

    '''expander'''

    def click_goals_expander(self):
        self.find_element(self.loc_goal_expander).click()

    def click_tasks_expander(self):
        expander = self.find_elements(self.loc_tasks_expander)[0]
        expander.click()

    def click_my_mentee_expander(self):
        self.find_element(self.loc_my_mentee_expander).click()

    ''' mentee menu '''

    def click_search_mentor(self):
        self.find_element(self.loc_search_mentor).click()

    ''' menu accesss'''

    def click_bulletin_menu(self):
        self.wait_for_overlay_dissapear()
        self.find_element(self.loc_bulletin_menu).click()

    def click_messages_menu(self):
        self.wait_for_overlay_dissapear()
        self.find_element(self.loc_messages_menu).click()

    def click_my_connection_menu(self):
        self.wait_for_overlay_dissapear()
        self.find_element(self.loc_my_connection_menu).click()

    def click_project_menu(self):
        self.wait_for_overlay_dissapear()
        self.find_element(self.loc_projects_menu).click()

    def click_logout_button(self):
        self.wait_for_overlay_dissapear()
        self.find_element(self.loc_log_out_menu).click()