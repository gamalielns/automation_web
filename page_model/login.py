from page_model import BasePage
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
import time

class Login(BasePage):

    loc_email = (By.ID, "Email")
    loc_password = (By.ID, "Password")
    loc_login_button = (By.XPATH, "//button[@class='btn btn-azure btn-lg btn-block']")
    loc_forgot_password = (By.XPATH, "//a[contains(text(),'Forgot your password')]")
    loc_invalid_credentials = (By.XPATH, "//li[contains(text(),'Invalid credentials')]")
    loc_email_empty = (By.XPATH, "//li[contains(text(),'The Email field is required.')]")
    loc_password_empty = (By.XPATH, "//li[contains(text(),'The Password field is required.')]")
    loc_select_company = (By.XPATH, "//span[@class='k-input']")
    loc_company_selected = (By.XPATH, "//*[contains(text(), 'Auto Mentifi - Admin (Mentifi)')]")
    loc_company_ok_button = (By.ID, "btnSubmit")

    def __init__(self):
        super().__init__()

    def verify_login_page_elements(self):
        login_element = [self.loc_email, self.loc_password, self.loc_login_button, self.loc_forgot_password]
        self.verify_elements(elements=login_element)

    def input_email_address(self, email):
        print("\n[LOGIN PAGE] input email data : {}".format(email))
        self.find_element(self.loc_email).clear()
        self.find_element(self.loc_email).send_keys(email)

    def input_password(self, password):
        print("[LOGIN PAGE] input password data : {}".format(password))
        self.find_element(self.loc_password).send_keys(password)

    def click_login_button(self):
        print("[LOGIN PAGE] click login button")
        self.find_element(self.loc_login_button).click()

    def enter_login_button(self):
        print("[LOGIN PAGE] enter login")
        self.find_element(self.loc_login_button).send_keys(Keys.ENTER)

    def click_forgot_password(self):
        print("[LOGIN PAGE] click forgot password button")
        self.find_element(self.loc_forgot_password).click()

    def verify_invalid_credentials(self):
        invalid = self.find_element(self.loc_invalid_credentials)
        print("[LOGIN PAGE] login response : {}".format(invalid.text))

    def verify_email_empty(self):
        invalid = self.find_element(self.loc_email_empty)
        print("[LOGIN PAGE] login response : {}".format(invalid.text))

    def verify_password_empty(self):
        invalid = self.find_element(self.loc_password_empty)
        print("[LOGIN PAGE] login response : {}".format(invalid.text))