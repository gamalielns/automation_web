from page_model import BasePage
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
import time

class Logout(BasePage):

    loc_logout = (By.XPATH, "//span[contains(text(),'Logout')]")
    loc_password = (By.ID, "Password")
    loc_login_button = (By.XPATH, "//button[@class='btn btn-azure btn-lg btn-block']")

    def __init__(self):
        super().__init__()

    def click_logout(self):
        self.wait_for_overlay_dissapear()
        self.find_element(self.loc_logout).click()
