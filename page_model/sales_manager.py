from page_model import BasePage
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
import time

class Login(BasePage):

    loc_email = (By.ID, "Email")
    loc_password = (By.ID, "Password")
    loc_login_button = (By.XPATH, "//button[@class='btn btn-azure btn-lg btn-block']")
    loc_forgot_password = (By.XPATH, "//a[contains(text(),'Forgot your password')]")
    loc_invalid_credentials = (By.XPATH, "//li[contains(text(),'Invalid credentials')]")
    loc_email_empty = (By.XPATH, "//li[contains(text(),'The Email field is required.')]")
    loc_password_empty = (By.XPATH, "//li[contains(text(),'The Password field is required.')]")
    loc_select_company = (By.XPATH, "//span[@class='k-input']")
    loc_company_selected = (By.XPATH, "//*[contains(text(), 'Auto Mentifi - Admin (Mentifi)')]")
    loc_company_ok_button = (By.ID, "btnSubmit")

    def __init__(self):
        super().__init__()

    def check_sales_manager(self):
        login_element = [self.loc_email, self.loc_password, self.loc_login_button, self.loc_forgot_password]
        self.verify_elements(elements=login_element)