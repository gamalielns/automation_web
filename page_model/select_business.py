from page_model import BasePage
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
import time

class SelectCompany(BasePage):

    loc_select_company = (By.XPATH,"/html/body/div[1]/div/div/div/div[1]/span/span/span[2]")
    loc_select_company_label = (By.XPATH, "//h1[@class='text-center']")
    loc_ok_button = (By.ID, "btnSubmit")

    def __init__(self):
        super().__init__()

    def verify_select_business_page_elements(self):
        self.wait_clickable(self.loc_ok_button)
        elements = [self.loc_ok_button, self.loc_select_company, self.loc_select_company_label]
        self.verify_elements(elements)

    def select_company(self,alias):
        self.verify_select_business_page_elements()
        time.sleep(5)
        self.find_element(self.loc_select_company).click()
        profile_parent = self.driver.find_element(By.ID,'cboAccount_listbox')
        profiles = profile_parent.find_elements(By.TAG_NAME, 'li')
        time.sleep(5)
        profiles[alias].click()

    def click_ok_button(self):
        animation = (By.XPATH, "//div[@class='k-animation-container'")
        WebDriverWait(self.driver, 10).until(ec.invisibility_of_element_located(animation))
        button = WebDriverWait(self.driver, 10).until(ec.element_to_be_clickable(self.loc_ok_button))
        WebDriverWait(self.driver, 10).until(ec.invisibility_of_element_located(animation))
        self.find_element(self.loc_ok_button).click()
        print('login successful)')