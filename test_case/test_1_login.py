import page_model
from test_data.user_data import User

login = page_model.Login()
# dashboard = page_model.Dashboard()
# forgot_password = page_model.ForgotPassword()
select_company = page_model.SelectCompany()
user = User()


class TestLogin():

    # def test_all_page_element_is_loaded(self):
    #     login.verify_login_page_elements()

    # #1  test_login using registered email & correct password (admin)
    # def test_admin_login_with_valid_credentials(self, ):
    #     login.input_email_address(user.admin_email)
    #     login.input_password(user.password)
    #     login.click_login_button()
    #     # select_company.select_company(0)
    #     # select_company.click_ok_button()
    #     dashboard.verify_information_pop_up()
    #     dashboard.verify_release_notes_pop_up()
    #     dashboard.verify_admin_dashboard()
    #     dashboard.get_logged_user_name()
    #     dashboard.click_logout_button()

    #1  test_login using registered email & correct password (mentor)
    def test_mentor_login_with_valid_credentials(self):
        login.input_email_address(user.admin_email)
        login.input_password(user.password)
        login.click_login_button()
        select_company.select_company(1)
        select_company.click_ok_button()
        # dashboard.verify_information_pop_up()
        # dashboard.verify_release_notes_pop_up()
        # dashboard.verify_mentor_dashboard()
        # dashboard.get_logged_user_name()
        # dashboard.click_logout_button()