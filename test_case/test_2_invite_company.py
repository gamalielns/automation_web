import page_model
import time
import csv
from test_data.user_data import User
from test_data.dummy_data import DummyData

login = page_model.Login()
network = page_model.BNetwork()
dashboard = page_model.Dashboard()
logout = page_model.Logout()
# forgot_password = page_model.ForgotPassword()
select_company = page_model.SelectCompany()
user = User()
dummy = DummyData()



class TestInviteCompany():

    def test_join_invitation_left_form(self):
            login.input_email_address(user.admin_email)
            login.input_password(user.password)
            login.click_login_button()
            time.sleep(3)
            select_company.select_company(1)
            time.sleep(1)
            select_company.click_ok_button()
            dashboard.verify_information_pop_up()
            dashboard.verify_release_notes_pop_up()
            time.sleep(1)
            network.invite_left_form(dummy.invite_email,user.gmail,dummy.name,user.gmail_pass,dummy.company_name,dummy.first_name,dummy.last_name,user.password,user.repassword,user.salutation,user.location)
            # login.input_email_address(dummy.invite_email)
            # login.input_password(user.password)
            # login.click_login_button()
            # dashboard.verify_information_pop_up()
            # dashboard.verify_release_notes_pop_up()
            # time.sleep(1)
            # logout.click_logout()

            # dashboard.verify_information_pop_up()
    # def test_join_invitation_right_form(self):
    #         login.input_email_address(user.admin_email)
    #         login.input_password(user.password)
    #         login.click_login_button()
    #         time.sleep(3)
    #         select_company.select_company(1)
    #         time.sleep(1)
    #         select_company.click_ok_button()
    #         dashboard.verify_information_pop_up()
    #         dashboard.verify_release_notes_pop_up()
    #         time.sleep(1)
    #         network.invite_right_form(dummy.invite_email, user.gmail, dummy.name, user.gmail_pass, dummy.company_name,dummy.first_name, dummy.last_name, user.password, user.repassword,user.salutation, user.location,user.link_email)
    #         logout.click_logout()