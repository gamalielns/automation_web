from faker import Faker
from test_data.get_time import GetTime


fake = Faker()
time = GetTime()


class DummyData():
    '''
    for generating fake data
    '''

    '''name'''
    first_name = fake.first_name()
    last_name = fake.last_name()
    full_name = fake.name()

    '''company'''
    company_name = fake.company()
    company_slogan = fake.catch_phrase()
    job_title = fake.job()

    '''address'''
    address = fake.address()
    country = fake.country()
    email = "{}.{}@mailinator.com".format(first_name, last_name)
    phone_numb = fake.msisdn()

    '''sentences'''
    words_100 = fake.sentence(nb_words=100)
    char_500 = fake.text(max_nb_chars=500)
    char_100 = fake.text(max_nb_chars=100)
    word_1 = fake.sentence(nb_words=1)

    '''current time'''
    curr_time = time.get_current_datetime()

    '''title'''
    title = fake.sentence(nb_words=3)[:-1]+" auto_"+curr_time

    '''invite email'''
    invite_email = 'gamns04+'+word_1+'@gmail.com'
    name = word_1
    def __init__(self):
        pass

if __name__ == '__main__':
    data = DummyData()
    print(data.title)