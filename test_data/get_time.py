from datetime import datetime

class GetTime(object):


    def get_current_datetime(self, hour_format_12=False):
        '''
        get current datetime in 24H format, return value only until second.
        Support for 12H format, change hour_format_12=True
        '''

        now = datetime.now()
        if hour_format_12==False:
            return now.strftime('%d/%m/%Y %H:%M:%S')
        else:
            return now.strftime('%d/%m/%Y %I:%M %p')

    def get_current_time(self, hour_format_12=False):
        '''
        get current time in 24H format, return value only until second.
        Support for 12H format, change hour_format_12=True
        '''
        raw = self.get_current_datetime(hour_format_12)
        #slice the string data so that only return hour minute data, ugly :(
        return raw[11:]

    def get_current_date(self):
        return datetime.now().date().strftime('%Y/%m/%d')



if __name__ == '__main__':
    time = GetTime()
    print(time.get_current_date())
